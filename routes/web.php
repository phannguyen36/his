<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/login', 'Auth\LoginController@getView')->name('login');
Route::post('/login', 'Auth\LoginController@login');

Route::group(['middleware' => ['permission:Dangky1080']], function () {
    Route::get('/regis', 'RegistrationController@getView')->name('regis');
    Route::post('/regis', 'RegistrationController@register');
    Route::post('/search', 'RegistrationController@search')->name('search');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/area/district', 'RegistrationController@getDistrict')->name('district');
    Route::get('/area/ward', 'RegistrationController@getWard')->name('ward');
    Route::post('/searchpatient', 'RegistrationController@getListWithCriteria')->name('searchpatient');
    Route::post('/delete', 'RegistrationController@deleteRegis')->name('delete');
    Route::get('/getpatient/{id}', 'RegistrationController@getPatient')->name('get');
    Route::post('/getpatient/{id}', 'RegistrationController@updatePatient')->name('update');
});

Route::group(['middleware' => ['permission:Laysinhhieu']], function () {
    Route::get('/signal', 'VitalSignalController@getView')->name('get-vital-signal-view');
    Route::get('/signal/{patientCode}', 'VitalSignalController@getView')->name('get-vital-signal-view-with-code');
    Route::post('/signal', 'VitalSignalController@createSignal')->name('add-vital-signal');
    Route::post('/signal/search', 'VitalSignalController@search')->name('vital-search');
    Route::post('/signal/{patientCode}', 'VitalSignalController@createSignal')->name('add-vital-signal-with-code');
});

Route::group(['middleware' => ['permission:Tamung']], function () {
    Route::get('/fee', 'HospitalFeeController@getView')->name('get-fee-view');
    Route::get('/fee/{patientCode}', 'HospitalFeeController@getView')->name('get-fee-view-with-code');
    Route::post('/fee/search', 'HospitalFeeController@search')->name('fee-search');
    Route::post('/fee', 'HospitalFeeController@createItem')->name('add-fee');
    Route::post('/fee/{patientCode}', 'HospitalFeeController@createItem')->name('add-fee-with-code');
});
<?php

namespace App\Service;

interface AuthAPIServiceInterface extends AbstractServiceInterface
{
    public function login($username, $password);
    public function refreshToken();
}

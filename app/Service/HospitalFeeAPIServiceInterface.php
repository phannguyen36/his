<?php

namespace App\Service;

interface HospitalFeeAPIServiceInterface extends AbstractServiceInterface
{
    public function getAllBooks();
    public function addFee(array $data);
    public function getAllFees($code);
}
<?php

namespace App\Service;

use App\Contracts\Client;
use App\Service\AuthAPIServiceInterface;

class AuthAPIServiceImpl extends AbstractServiceImpl implements AuthAPIServiceInterface {
    protected $client;

    public function __construct()
    {
        $this->client = app(Client::class);
    }

    public function login($username, $password)
    {
        $res = $this->client->post('staff/token', [
            'user_name' => $username,
            'password'  => $password,
        ]);
        return $res ? [$res->permissions , $res->token] : [false, ""];
    }

    public function refreshToken()
    {
        $res = $this->client->get('staff/refresh-token', $this->getDefaultHeader());
        return $res ? $res->token : false;
    }
}
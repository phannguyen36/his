<?php
namespace App\Service;

use App\Contracts\Client;
use Illuminate\Support\Facades\Log;

class GuzzleHttp implements Client
{
    protected $client;

    public function __construct($client)
    {
        $this->client = $client;
    }

    public function get($path, array $option = [])
    {
        return $this->parserData($this->client->get($path, $option));
    }

    public function post($path, array $data = [], array $option = [])
    {
        return $this->parserData(
            $this->client->post($path,
                array_merge(['json' => $data], $option)
            )
        );
    }

    public function put($path, array $data = [], array $option = [])
    {
        return $this->parserData(
            $this->client->put($path, array_merge(['json' => $data], $option))
        );
    }

    public function delete($path, array $option = [])
    {
        return $this->parserData($this->client->delete($path, $option));
    }

    public function getConfig($name)
    {
        return $this->client->getConfig($name);
    }

    private function parserData($res)
    {
        $statusCode = $res->getStatusCode();
        if (200 === $statusCode) {
            return json_decode($res->getBody());
        } else if (204 === $statusCode) {
            return true;
        }

        return false;
    }
}

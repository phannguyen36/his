<?php

namespace App\Service;

class AbstractServiceImpl implements AbstractServiceInterface {
    public function getDefaultHeader() {
        return [
            'headers' => [
                'X-AUTH-TOKEN' => request()->cookie('token', ''),
                'Content-Type' => 'application/json',
            ],
        ];
    }
}
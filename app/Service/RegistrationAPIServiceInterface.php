<?php

namespace App\Service;

interface RegistrationAPIServiceInterface extends AbstractServiceInterface
{
    public function register(array $data);
    public function getMasterList();
    public function getPatientInfo($code);
    public function getListRegistration();
    public function getAllAddress();
    public function search(array $data);
    public function delete($id);
    public function get($id);
    public function update(array $data);
}

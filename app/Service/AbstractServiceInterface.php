<?php

namespace App\Service;

interface AbstractServiceInterface {
    public function getDefaultHeader();
}
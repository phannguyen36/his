<?php

namespace App\Service;

use App\Contracts\Client;
use Illuminate\Http\Request;

class VitalSignalAPIServiceImpl extends AbstractServiceImpl implements VitalSignalAPIServiceInterface
{
    protected $client;

    public function __construct()
    {
        $this->client = app(Client::class);
    }

    public function addItem(array $data = [])
    {
        return $this->client->post('vital/items', $data, $this->getDefaultHeader());
    }

    public function getItems($code)
    {
        return $this->client->get('vital/items/' . htmlspecialchars($code), $this->getDefaultHeader());
    }
}
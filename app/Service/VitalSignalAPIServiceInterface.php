<?php

namespace App\Service;

interface VitalSignalAPIServiceInterface extends AbstractServiceInterface
{
    public function addItem(array $data);
    public function getItems($code);
}

<?php

namespace App\Service;

use App\Contracts\Client;

class HospitalFeeAPIServiceImpl extends AbstractServiceImpl implements HospitalFeeAPIServiceInterface
{
    protected $client;

    public function __construct()
    {
        $this->client = app(Client::class);
    }
    
    public function getAllBooks() {
        return $this->client->get('fee/books', $this->getDefaultHeader());
    }

    public function addFee(array $data = []) {
        return $this->client->post('fee/add-item', $data, $this->getDefaultHeader());
    }

    public function getAllFees($code) {
        return $this->client->get('fee/all-items/' . $code, $this->getDefaultHeader());
    }
}
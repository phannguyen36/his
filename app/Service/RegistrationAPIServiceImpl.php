<?php

namespace App\Service;

use App\Contracts\Client;
use Illuminate\Http\Request;

class RegistrationAPIServiceImpl extends AbstractServiceImpl implements RegistrationAPIServiceInterface
{
    protected $client;

    public function __construct()
    {
        $this->client = app(Client::class);
    }

    public function getMasterList()
    {
        return $this->client->get('general/master-list', $this->getDefaultHeader());
    }

    public function register(array $data = [])
    {
        return $this->client->post('registration', $data, $this->getDefaultHeader());
    }

    public function getPatientInfo($code)
    {
        return $this->client->get('patient/' . htmlspecialchars($code), $this->getDefaultHeader());
    }

    public function getListRegistration()
    {
        return $this->client->get('registration', $this->getDefaultHeader());
    }

    public function getAllAddress()
    {
        return $this->client->get('general/address-list', $this->getDefaultHeader());
    }

    public function delete($id)
    {
        return $this->client->delete('registration/' . $id, $this->getDefaultHeader());
    }

    public function search(array $data = [])
    {
        return $this->client->post('registration/search', $data, $this->getDefaultHeader());
    }

    public function get($id) 
    {
        return $this->client->get('registration/' . $id, $this->getDefaultHeader());
    }

    public function update(array $data = []) 
    {
        return $this->client->put('registration', $data, $this->getDefaultHeader());
    }
}

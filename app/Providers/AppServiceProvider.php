<?php

namespace App\Providers;

use App\Service\AbstractServiceImpl;
use App\Service\AuthAPIServiceImpl;
use App\Service\HospitalFeeAPIServiceImpl;
use App\Service\RegistrationAPIServiceImpl;
use App\Service\VitalSignalAPIServiceImpl;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Service\AbstractServiceInterface', function(Application $app) {
            return new AbstractServiceImpl();
        });
        $this->app->bind('App\Service\AuthAPIServiceInterface', function(Application $app) {
            return new AuthAPIServiceImpl();
        });
        $this->app->bind('App\Service\RegistrationAPIServiceInterface', function(Application $app) {
            return new RegistrationAPIServiceImpl();
        });
        $this->app->bind('App\Service\VitalSignalAPIServiceInterface', function(Application $app) {
            return new VitalSignalAPIServiceImpl();
        });
        $this->app->bind('App\Service\HospitalFeeAPIServiceInterface', function(Application $app) {
            return new HospitalFeeAPIServiceImpl();
        });
    }
}

<?php

namespace App\Providers;

use App\Contracts\Client;
use App\Service\GuzzleHttp;
use Illuminate\Support\ServiceProvider;

class ClientServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Client::class, function () {
            return new GuzzleHttp(
                new \GuzzleHttp\Client([
                    'base_uri' => config('app.api')
                ])
            );
        });
    }

    public function provides()
    {
        return [Client::class];
    }
}

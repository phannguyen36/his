<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = 'xxx')
    {
        $permissions = session('permission');
        if (strpos($permissions, $permission) !== false) {
            return $next($request);
        }
        abort(404);
    }
}

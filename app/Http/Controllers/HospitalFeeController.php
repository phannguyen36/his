<?php

namespace App\Http\Controllers;

use App\Service\HospitalFeeAPIServiceInterface;
use App\Service\RegistrationAPIServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HospitalFeeController extends Controller
{
    public function __construct(RegistrationAPIServiceInterface $regisService, HospitalFeeAPIServiceInterface $feeService)
    {
        $this->middleware('guest')->except('logout');
        $this->externalService = $feeService;
        $this->regisService = $regisService;
    }

    public function search(Request $req)
    {
        $code = $req->code;
        $info = $this->regisService->getPatientInfo($code);
        $items = $code == '' ? [] : $this->externalService->getAllFees($code);
        if ($info) {
            $info->items = $items;
            return json_encode($info);
        }
        return "";
    }

    public function getView($patientCode = '')
    {
        $books = $this->externalService->getAllBooks();
        $items = $patientCode == '' ? [] : $this->externalService->getAllFees($patientCode);
        return view('addHospitalFee')->with(compact('patientCode', 'books', 'items'));
    }

    public function createItem(Request $req) {
        $data = [];
        $patientCode = $req->input('code');
        $data['ma_bn'] = $patientCode;
        $data['quyen_so'] = (int) $req->input('book_id');
        $data['so_tien'] = (float) $req->input('amount');

        $result = $this->externalService->addFee($data);

        return redirect()->route('get-fee-view-with-code', ['patientCode' => $patientCode]);
    }
}
<?php

namespace App\Http\Controllers;

use App\Service\RegistrationAPIServiceInterface;
use App\Service\VitalSignalAPIServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VitalSignalController extends Controller
{
    public function __construct(RegistrationAPIServiceInterface $regisService, VitalSignalAPIServiceInterface $vitalSignalService)
    {
        $this->middleware('guest')->except('logout');
        $this->vitalSignalService = $vitalSignalService;
        $this->regisService = $regisService;
    }

    public function getView($patientCode = '')
    {
        return view('addSignal')->with(compact('patientCode'));
    }

    public function search(Request $req)
    {
        $code = $req->code;
        $info = $this->regisService->getPatientInfo($code);
        $items = $this->vitalSignalService->getItems($code);
        if ($info) {
            $info->history = $items;
            return json_encode($info);
        }
        return "";
    }

    public function createSignal(Request $req) {
        $data = [];
        $patientCode = $req->input('code');
        $data['patient_id'] = $patientCode;
        $data['sense'] = $req->input('sense');
        $data['pulse'] = $req->input('pulse');
        $data['temperature'] = $req->input('temperature');
        $data['blood_pressured'] = $req->input('blood_pressured1') . "/" . $req->input('blood_pressured2');
        $data['weight'] = $req->input('weight');
        $data['height'] = $req->input('height');
        $data['bmi'] = $req->input('bmi');
        $data['breathing_rate'] = $req->input('breathing_rate');

        $result = $this->vitalSignalService->addItem($data);

        return redirect()->route('get-vital-signal-view-with-code', ['patientCode' => $patientCode]);
    }
}
<?php

namespace App\Http\Controllers;

use App\Service\RegistrationAPIServiceInterface;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    const DELIMITER = '~';
    public function __construct(RegistrationAPIServiceInterface $externalService)
    {
        $this->middleware('guest')->except('logout');
        $this->externalService = $externalService;
    }

    public function getView()
    {
        $data         = $this->externalService->getMasterList();
        $specialities = array_reduce($data->specialists, function (&$result, $item) {
            $result[$item->code] = $item->full_name;
            return $result;
        });
        // $specialities = [];
        $doctors = array('' => '') + array_reduce($data->doctors, function (&$result, $item) {
            $result[$item->code] = $item->full_name;
            return $result;
        });
        // $doctors = [];
        $registrations = $this->externalService->getListRegistration();
        // $registrations = [];
        $addressData = $this->getAllAddress();
        $provinces   = array_reduce($addressData, function (&$result, $item) {
            $result[$item['province_id']] = $item['province_name'];
            return $result;
        });
        // $provinces = [];
        $districts = array_reduce($addressData[0]['districts'], function (&$result, $item) {
            $result[$item['district_id']] = $item['district_name'];
            return $result;
        });
        // $districts = [];
        $wards = array_reduce($addressData[0]['districts'][0]['wards'], function (&$result, $item) {
            $result[$item['ward_id']] = $item['ward_name'];
            return $result;
        });
        $wards = $wards ? $wards : [];
        return view('registration')->with(compact('specialities', 'doctors', 'registrations',
            'provinces', 'districts', 'wards', 'addressData'));
    }

    public function register(Request $req)
    {
        $onlreg = $this->buildOnlineRegis($req);
        if ($this->externalService->register($onlreg)) {

        }
        return redirect()->route('regis');
    }

    public function search(Request $req)
    {
        $info = $this->externalService->getPatientInfo($req->code);
        return $info ? json_encode($info) : "";
    }

    public function getDistrict(Request $req)
    {
        $provinceId = $req->value;
        $data       = $this->getAllAddress();
        $items      = [];
        foreach ($data as $item) {
            if ($item['province_id'] == $provinceId) {
                $items = $item['districts'];
                break;
            }
        }
        $resultItems = [];
        foreach ($items as $item) {
            array_push($resultItems, (object) array(
                'item'  => $item['district_id'],
                'value' => $item['district_name']));
        }
        return $resultItems;
    }

    public function getWard(Request $req)
    {
        $districtId = $req->value;
        $data       = $this->getAllAddress();
        $items      = [];
        foreach ($data as $item) {
            foreach ($item['districts'] as $district) {
                if ($district['district_id'] == $districtId) {
                    $items = $district['wards'];
                    break;
                }
            }
            if (!empty($items)) {
                break;
            }
        }
        $resultItems = [];
        foreach ($items as $item) {
            array_push($resultItems, (object) array(
                'item'  => $item['ward_id'],
                'value' => $item['ward_name']));
        }
        return $resultItems;
    }

    public function getListWithCriteria(Request $request)
    {
        $fromDate = $request->from;
        $toDate   = $request->to;
        $name     = $request->name;
        $obj      = $this->buildSearchObj($fromDate, $toDate, $name);

        return $this->externalService->search($obj);
    }

    public function deleteRegis(Request $request)
    {
        $id = $request->id;
        return $this->externalService->delete($id);
    }

    public function getPatient(Request $request)
    {
        $id      = $request->id;
        $patient = $this->convertToFrontentObj($this->externalService->get($id));

        $data         = $this->externalService->getMasterList();
        $specialities = array_reduce($data->specialists, function (&$result, $item) {
            $result[$item->code] = $item->full_name;
            return $result;
        });
        // $specialities = [];
        $doctors = array('' => '') + array_reduce($data->doctors, function (&$result, $item) {
            $result[$item->code] = $item->full_name;
            return $result;
        });
        // $doctors     = [];
        $addressData = $this->getAllAddress();
        $provinces   = array_reduce($addressData, function (&$result, $item) {
            $result[$item['province_id']] = $item['province_name'];
            return $result;
        });

        $rightProvince      = null;
        $rightProvinceIndex = 0;
        foreach ($addressData as $key => $value) {
            if ($value['province_id'] == $patient['province']) {
                $rightProvinceIndex = $key;
                $rightProvince      = $value;
                break;
            }
        }

        $districts = array_reduce($rightProvince['districts'], function (&$result, $item) {
            $result[$item['district_id']] = $item['district_name'];
            return $result;
        });

        $rightDistrict = null;
        foreach ($addressData[$rightProvinceIndex]['districts'] as $value) {
            if ($value['district_id'] == $patient['district']) {
                $rightDistrict = $value;
                break;
            }
        }

        $wards = array_reduce($rightDistrict['wards'], function (&$result, $item) {
            $result[$item['ward_id']] = $item['ward_name'];
            return $result;
        });
        $wards = $wards ? $wards : [];

        return view('editRegistration')->with(compact('specialities', 'doctors', 'provinces',
            'districts', 'wards', 'addressData', 'patient'));
    }

    public function updatePatient(Request $request)
    {
        $data                = $this->buildOnlineRegis($request);
        $data['register_id'] = intval($request->id);

        if ($this->externalService->update($data)) {

        }
        return redirect()->route('regis');
    }

    private function buildSearchObj($from, $to, $name)
    {
        $data                       = [];
        $data['req_exam_date_from'] = $from ? date_format(date_create_from_format('d/m/Y', $from), 'Y-m-d') : null;
        $data['req_exam_date_to']   = $to ? date_format(date_create_from_format('d/m/Y', $to), 'Y-m-d') : null;
        $data['patient_name']       = $name;
        $data['dept_id']            = null;
        $data['req_exam_date']      = null;

        return $data;
    }

    private function buildOnlineRegis(Request $request)
    {
        $result  = [];
        $patient = [];

        $familyPatients = [];
        array_push($familyPatients, $this->generateFamilyPatient("CHA", $request->father));
        array_push($familyPatients, $this->generateFamilyPatient("ME", $request->mother));
        array_push($familyPatients, $this->generateFamilyPatient("OTHER", $request->other));

        $result['family_patients'] = $familyPatients;
        $result['relationship']    = "Bản thân";
        $result['patient_id']      = $request->code;
        $result['patient_name']    = $request->fullname;
        $result['date_birth']      = date_format(date_create_from_format('d/m/Y', $request->birthday), 'Y-m-d');
        $result['gender_id']       = $request->gender;
        $result['cellphone']       = $request->phone;
        $result['email']           = $request->email;
        $result['address']         = $request->address;
        $result['province_id']     = $request->province;
        $result['district_id']     = $request->district;
        $result['ward_id']         = $request->ward;

        $result['special_id'] = $request->speciality;
        // $result['speciality_name']    = $specArr[1];
        $result['staff_id'] = $request->doctor;
        // $result['doctor_name']        = $docArr[1];
        $reqExam                      = date_create_from_format('d/m/Y', $request->date);
        $result['req_exam_date']      = date_format($reqExam, 'Y-m-d');
        $result['req_exam_time']      = date_format(date_create($request->time), 'H:i');
        $result['speciality_room_no'] = "";
        $result['wage']               = 100000;
        $result['reg_fee']            = 10000;
        $result['myself']             = 0;
        $result['clinical_symptoms']  = $request->reason;

        return $result;
    }

    private function convertToFrontentObj($obj)
    {
        $result               = [];
        $result['id']         = $obj->register_id;
        $result['code']       = $obj->patient_id;
        $result['fullname']   = $obj->patient_name;
        $result['birthday']   = date_format(date_create($obj->date_birth), 'm/d/Y');
        $result['gender']     = $obj->gender_id;
        $result['phone']      = $obj->cellphone;
        $result['email']      = $obj->email;
        $result['address']    = $obj->address;
        $result['province']   = $obj->province_id;
        $result['district']   = $obj->district_id;
        $result['ward']       = $obj->ward_id;
        $result['date']       = date_format(date_create($obj->req_exam_date), 'm/d/Y');
        $result['time']       = date_format(date_create($obj->req_exam_time), 'H:i');
        $result['reason']     = $obj->clinical_symptoms;
        $result['speciality'] = $obj->special_id;
        $result['doctor']     = $obj->staff_id;
        $result['father']     = $this->getFamilyMemberName("CHA", $obj->family_patients);
        $result['mother']     = $this->getFamilyMemberName("ME", $obj->family_patients);
        $result['other']      = $this->getFamilyMemberName("OTHER", $obj->family_patients);
        return $result;

    }

    private function getAllAddress()
    {
        return json_decode(file_get_contents("address.json"), true);
    }

    private function generateFamilyPatient($code, $name)
    {
        return array(
            'family_relationship' => $code,
            'family_name'         => $name,
        );
    }

    private function getFamilyMemberName($code, array $items = [])
    {
        foreach ($items as $item) {
            if ($code == $item->family_relationship) {
                return $item->family_name;
            }
        }
        return "";
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Service\AuthAPIServiceInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';

    private $externalService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthAPIServiceInterface $externalService)
    {
        // $this->middleware('guest')->except('logout');
        $this->externalService = $externalService;
    }

    public function getView()
    {
        return view('login');
    }

    public function username()
    {
        return 'username';
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (list($permissions, $token) = $this->externalService->login($request->username, $request->password)) {
            return $this->sendSuccessLogin($request, $token, $permissions);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $cookie = \Cookie::forget('token');
        return redirect()->route('login')->withCookie($cookie);
    }

    private function sendSuccessLogin(Request $request, $token, $permissions = "")
    {
        $request->session()->regenerate();
        session(['permission' => $permissions]);

        $this->clearLoginAttempts($request);

        return redirect()->intended($this->redirectPath())->withCookie(cookie()->make('token', $token, 60));
    }
}

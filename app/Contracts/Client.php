<?php

namespace App\Contracts;

interface Client
{
    public function get($path, array $options);
    public function post($path, array $data, array $options);
    public function put($path, array $data = [], array $option = []);
    public function delete($path, array $option = []);
    public function getConfig($name);
}

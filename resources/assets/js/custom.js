function getDate($date) {
    var dd = $date.getDate();
    var mm = $date.getMonth() + 1; //January is 0!

    var yyyy = $date.getFullYear();
    dd = addZero(dd);
    mm = addZero(mm);
    return dd + '/' + mm + '/' + yyyy;
};

function getDateTime($date) {
    var date = getDate($date);
    var hh = addZero($date.getHours());
    var mm = addZero($date.getMinutes());
    return date + ' ' + hh + ':' + mm;
};

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function parseDate(event) {
    var inputLength = event.target.value.length;
    if (inputLength === 2 || inputLength === 5) {
        var thisVal = event.target.value;
        thisVal += '/';
        $(event.target).val(thisVal);
    }
};

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
};

function isFloatNumber(evt) {
    if (isNumber(evt)) {
        return true;
    }
    var value = event.target.value + event.key;
    var decimal=  /^[0-9]*\.?[0-9]*$/; 
    if(value.match(decimal)) { 
        return true;
    }
    return false;
};

function fnsearch() {
    $('.search').click();
};
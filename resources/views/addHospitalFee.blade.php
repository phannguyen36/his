@extends('core.master')

@section('page-name')
Viện phí
@endsection

@php
    $globalDateFormat = 'd/m/Y H:i';
@endphp

@section('css')
<style type="text/css">
    .add-fee {
        width: 100%;
        margin-top: 10px;
        height: 100px;
    }
    .history {
        font-family:sans-serif;
        margin:0;
        padding:20px 0 0;
    }
    .result {
        padding: 0 25px;
    }

</style>
@endsection

@section('main')
    <div>
        {!! BootForm::open() !!}
      
		<div class="panel">
		<table class="table-responsive">
                        <tbody>
                            <tr>
							<td style="padding-left:10px;padding-top:10px">Mã Bệnh Nhân</td>
							</tr>
							<tr>
								<td style="width:650px;padding-left:10px;padding-right:10px">
									 <input type="text" name="code" id="code" class="form-control input-60 with-glyphicon" onkeypress="Javascript: if (event.keyCode==13) fnsearch(); else return isNumber(event);" value={{ $patientCode }}>
                                    <a class="search" data-url="{{ route('fee-search') }}">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </a>
                                
								</td>
							</tr>
							<tr>
							    <td style="padding-left:10px;padding-top:5px">Họ Tên</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px">{!! BootForm::text('', 'fullname')->disable() !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:10px">Ngày sinh</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px">
									{!! BootForm::text('', 'birthday')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '10')->disable() !!}
								</td>
                            </tr>
							<tr>
							   <td style="padding-left:10px">Địa chỉ</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:10px">
								{!! BootForm::text('', 'address')->disable() !!}
							</td>
							</tr>
							
							
                        </tbody>
                    </table>
		</div>

		<div class="panel">
		<table class="table-responsive">
                        <tbody>
                            <tr>
							<td style="padding-left:10px;padding-top:10px">Ký hiệu biên lai</td>
							</tr>
							<tr>
								<td style="width:650px;padding-left:10px;padding-right:10px">
									  {!! BootForm::text('', 'book')->readOnly() !!}
									  {!! BootForm::hidden('book_id') !!}
								</td>
							</tr>
							<tr>
							    <td style="padding-left:10px;padding-top:5px">Số biên lai</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px">{!! BootForm::text('', 'bookNumber')->disable() !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:10px">Số tiền (VND)</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px">
									{!! BootForm::text('', 'amount') !!}
								</td>
                            </tr>
							<tr>
							   <td style="padding-left:10px">Ngày thu</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:10px">
								{!! BootForm::text('', 'regisDate')->required()->readOnly()->value(Carbon\Carbon::now()->format('d/m/y H:i')) !!}
							</td>
							</tr>
							<tr>
							    <td style="padding-left:10px;padding-top:5px;padding-bottom:10px">
								<button type="submit" class="btn">Lưu & In</button>
								<button type="button" class="btn regis-cancel">Huỷ</button>
								</td>
								
							</tr>
							
                        </tbody>
                    </table>
		</div>
		
        {!! BootForm::close() !!}
    </div>

    <div class="row result">
        <div id="history" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 history">
            <h4 class="box-title">Lịch sử viện phí</h4>
            <table class="table table-bordered">
                    <thead>
                        <th>Ngày</th>
                        <th>Số tiền</th>
                        <th>Sổ</th>
                        <th>Biên lai</th>
                    </thead>
                    <tbody>
                        @if(isset($items))
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ Carbon\Carbon::parse($item->ngay)->format($globalDateFormat)  }}</td>
                                    <td>{{ $item->so_tien }}</td>
                                    <td>{{ $item->so->so_hieu }}</td>
                                    <td>{{ $item->so_bien_lai }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
        </div>
    </div>

@endsection
<script src="{{ asset('js/mobileSelect.js') }}"></script>
@section('js')
<script>
$(document).ready(function () {
    var token = $('input[name="_token"]').attr('value');
    $('.search').on('click', function(){
        var $this = $(this),
          url = $this.data('url'),
          code = $("#code").val();

        $.post(url, {
            _token: token,
            code: code
        }).done(function(data) {
            if (data) {
                var obj = JSON.parse(data);
                $("#fullname").val(obj.full_name);
                $("#birthday").val(getDate(new Date(obj.date_of_birthday)));
                $("#address").val(obj.full_address);

                var rows = "";
                $("#history tr").remove(); 
                $.each(obj.items, function(){
                    rows += "<tr><td>" + this.ngay + "</td><td>" + this.so_tien + "</td><td>" + this.so.so_hieu + "</td><td>" + this.so_bien_lai + "</td></tr>";
                });

                $( rows ).appendTo( "#history tbody" );
            }
        });
    });

    const patientCode = $('#code').val();
    if (patientCode) {
        $('.search').trigger('click');
    }

    const bookArr = [];

    <?php foreach($books as $key => $val){ ?>
        var item = {};
        item.id = '<?php echo $val->id ?>';

        item.number = '<?php echo $val->so_ghi ?>';
        item.value = '<?php echo $val->so_hieu ?>';

        bookArr.push(item);
    <?php } ?>

    new MobileSelect({
    trigger: '#book',
    title: 'Ký hiệu biên lai',
    cancelBtnText: 'Huỷ',
    ensureBtnText: 'Chọn',
    wheels: [
        {
            data: bookArr
        },
    ],
    // position:[1, 2],
    callback:function(indexArr, data) {
        if (data) {
            const res = data[0];
            console.log(res);
            $('#book').val(res.value);
            $('#bookNumber').val(res.number);
            $("input[name=book_id]").val(res.id);
        }
    }
    });
});

</script>
@endsection

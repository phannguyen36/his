@extends('core.master')

@section('page-name')
Lấy sinh hiệu
@endsection

@section('css')
<style type="text/css">
    .add-signal {
        width: 100%;
        margin-top: 10px;
        height: 100px;
    }
    .history {
        font-family:sans-serif;
        margin:0;
        padding:20px 0 0;
    }
    .list,
    .list ul {
      margin:0 0 0 1em; /* indentation */
      padding:0;
      list-style:none;
      color:#369;
      position:relative;
    }

    .list ul {margin-left:.5em} /* (indentation/2) */

    .list:before,
    .list ul:before {
      content:"";
      display:block;
      width:0;
      position:absolute;
      top:0;
      bottom:0;
      left:0;
      border-left:1px solid;
    }

    .list li {
      margin:0;
      padding:0 1.5em; /* indentation + .5em */
      line-height:2em; /* default list item's `line-height` */
      font-weight:bold;
      position:relative;
    }

    .list li:before {
      content:"";
      display:block;
      width:10px; /* same with indentation */
      height:0;
      border-top:1px solid;
      margin-top:-1px; /* border top width */
      position:absolute;
      top:1em; /* (line-height/2) */
      left:0;
    }

    .list li:last-child:before {
      background:#ecf0f5; /* same with body background */
      height:auto;
      top:1em; /* (line-height/2) */
      bottom:0;
    }

    .result {
        padding: 0 25px;
    }
	
</style>
@endsection

@section('main')

    <div>
        {!! BootForm::open() !!}
		<div class="panel">
		<table class="table-responsive">
                        <tbody>
                            <tr>
							<td style="padding-left:10px;padding-top:10px">Mã Bệnh Nhân</td>
							</tr>
							<tr>
								<td style="width:650px;padding-left:10px">
									<input type="text" name="code" id="code" class="form-control input-60 with-glyphicon" onkeypress="Javascript: if (event.keyCode==13) fnsearch(); else return isNumber(event);" value={{ $patientCode }}>
									<a class="search" data-url="{{ route('vital-search') }}">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </a>
								</td>
							</tr>
							<tr>
							    <td style="padding-left:10px;padding-top:5px">Họ Tên</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px">{!! BootForm::text('', 'fullname')->disable() !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:10px">Ngày Sinh</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px">
									{!! BootForm::text('', 'birthday')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '10')->disable() !!}
								</td>
                            </tr>
							<tr>
							   <td style="padding-left:10px">Địa Chỉ</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:10px">
								{!! BootForm::text('', 'address')->disable() !!}
							</td>
						</tr>
                        </tbody>
                    </table>
		</div>
		<div class="panel">
                    <table class="table-responsive">
                        <tbody>
                            <tr>
                                <td style="padding-left:10px;padding-top:10px">Mạch (lần/phút)</td>
							</tr>
							 <tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'pulse')->attribute('onkeypress', 'return isNumber(event)')->required()->id("pulse")->readOnly() !!}</td>
								</td>
							</tr>
							<tr>
							    <td style="padding-left:10px;padding-top:5px">Nhiệt độ</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'temperature')->required()->attribute('onkeypress', 'return isFloatNumber(event)')->attribute('maxlength', '10')->id("temperature")->readOnly() !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:10px;padding-top:5px">Huyết áp</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">
                                    {!! BootForm::text('', 'blood_pressured1') !!} / {!! BootForm::text('', 'blood_pressured2') !!}
                                </td>
                            </tr>
							<tr>
								 <td style="padding-left:10px;padding-top:5px">Nhịp thở (lần/phút)</td>
							</tr>
							<tr>
								 <td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'breathing_rate')->attribute('onkeypress', 'return isNumber(event)')->id("breathing_rate")->readOnly() !!}</td>
							</tr>
							<tr>
                                <td style="padding-left:10px;padding-top:5px">Cân nặng</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'weight')->attribute('onkeypress', 'return isFloatNumber(event)')->addClass('input') !!}</td>
							</tr>
							<tr>
							    <td style="padding-left:10px;padding-top:5px">Chiều cao</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'height')->required()->attribute('onkeypress', 'return isFloatNumber(event)')->attribute('maxlength', '10') !!}</td>
							</tr>
                            <tr>
                                <td style="padding-left:10px;padding-top:5px">BMI</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'bmi')->readOnly() !!}</td>
                            </tr>
							<tr>
								<td style="padding-left:10px;padding-top:5px">Tri giác</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'sense') !!}</td>
							</tr>
							<tr>
                               <td style="padding-left:10px;padding-top:5px">Ngày thực hiện</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:10px;width:650px">{!! BootForm::text('', 'regisDate')->required()->readOnly()->value(Carbon\Carbon::now()->format('d/m/y H:i')) !!}</td>
                            </tr>
							<tr>
								<td <td style="padding-left:10px;padding-top:5px;padding-bottom:10px"><button type="submit" class="btn add-signal" style="width:100px;height:30px;margin-top:1px">Xác nhận</button></td>
								
                        </tbody>
                    </table>
        </div>
		{!! BootForm::close() !!}
    </div>
	<div class="row result">
        {{-- <div id="history" class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <h4 class="box-title">Lịch sử sinh hiệu</h4>
            <ul class="list">
            </ul>
        </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <canvas id="signal-chart"></canvas>
        </div>
    </div>

										
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
<script src="{{ asset('bower_components/list.js/list.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/mobileSelect.js') }}"></script>
<script>
var config = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Nhiệt độ",
            backgroundColor: "red",
            borderColor: "red",
            data: [
            ],
            fill: false,
        }, {
            label: "Huyết áp",
            fill: false,
            backgroundColor: "blue",
            borderColor: "blue",
            data: [
            ],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:true,
            text:'Lịch sử sinh hiệu'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Ngày lấy'
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Giá trị'
                }
            }]
        }
    }
};

var options = {
  valueNames: [ 'created_date' ],
  item: '<li><p class="created_date"></p></li>'
};
var userList;

window.onload = function() {
    var ctx = document.getElementById("signal-chart").getContext("2d");
    window.myLine = new Chart(ctx, config);

    userList = new List('history', options, []);
};

function calculateBMI() {
    var height = $('#height').val(),
        weight = $('#weight').val();

    if (height != "" && weight != "") {
        height = height / 100; // convert to meter
        $('#bmi').val((weight / (height * height)).toFixed(2));
    }
    // return weight / (height * height);
}

$(document).ready(function () {
    var token = $('input[name="_token"]').attr('value');
    $('.search').on('click', function(){
        var $this = $(this),
          url = $this.data('url'),
          code = $("#code").val();

        $.post(url, {
            _token: token,
            code: code
        }).done(function(data) {
            if (data) {
                var obj = JSON.parse(data);
                $("#fullname").val(obj.full_name);
                $("#birthday").val(getDate(new Date(obj.date_of_birthday)));
                $("#address").val(obj.full_address);

                window.myLine.destroy();
                config.data.labels = [];
                config.data.datasets[0].data = [];
                config.data.datasets[1].data = [];
                var newItems = obj.history;
                newItems.forEach(function(element) {
                    element.created_date = getDateTime(new Date(element.created_date));
                    config.data.labels.push(element.created_date);
                    config.data.datasets[0].data.push(element.temperature);
                    config.data.datasets[1].data.push(element.blood_pressured);
                });

                if(Array.isArray(newItems) && newItems.length > 0) {
                    const lastItem = newItems[newItems.length - 1];
                    $('#weight').val(lastItem.weight);
                    $('#height').val(lastItem.height);
                    calculateBMI();
                } else {
                    $('#weight').val("");
                    $('#height').val("");
                    $('#bmi').val("");
                }

                // newItems.sort(function(a, b) {
                //     if(a.created_date < b.created_date) return 1;
                //     if(a.created_date > b.created_date) return -1;
                //     return 0;
                // });

                // userList.clear();
                // userList.add(newItems);

                var ctx = document.getElementById("signal-chart").getContext("2d");
                var chartData = [];
                window.myLine = new Chart(ctx, config);
            }
        });
    });

    const patientCode = $('#code').val();
    if (patientCode) {
        $('.search').trigger('click');
    }
    const pulseData = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200];
    new MobileSelect({
        trigger: '#pulse',
        title: 'Mạch (lần/phút)',
        cancelBtnText: 'Huỷ',
        ensureBtnText: 'Chọn',
        wheels: [
            {
                data: pulseData
            }
        ],
        position:[40],
        callback:function(indexArr, data){
            $('#pulse').val(data)
        }
    });

    const breathingRateData = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50];
    new MobileSelect({
        trigger: '#breathing_rate',
        title: 'Nhịp thở (lần/phút)',
        cancelBtnText: 'Huỷ',
        ensureBtnText: 'Chọn',
        wheels: [
            {
                data: breathingRateData
            }
        ],
        position:[10],
        callback:function(indexArr, data){
            $('#breathing_rate').val(data)
        }
    });

    new MobileSelect({
        trigger: '#temperature',
        title: 'Nhiệt độ',
        cancelBtnText: 'Huỷ',
        ensureBtnText: 'Chọn',
        wheels: [
                    {data: [35, 36, 37, 38, 39, 40, 41, 42, 43]},
                    {data: ['.0' ,'.1', '.2', '.3', '.4', '.5' ,'.6', '.7', '.8', '.9']}
                ],
        position:[2, 0],
        callback:function(indexArr, data) {
            $('#temperature').val(data.join(''));
        }
    });

    $("#weight").on('focusout', calculateBMI);
    $("#height").on('focusout', calculateBMI);

});

</script>
@endsection

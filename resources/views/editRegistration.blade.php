@extends('core.master')

@section('css')
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="https://harvesthq.github.io/chosen/chosen.css" rel="stylesheet">
@endsection

@section('page-name')
Đăng ký khám bệnh online
@endsection

@section('main')
<div class="his-container-edit">
        <div class="regis-body">
            @php
                $columnSizes = [
                  'sm' => [4, 7],
                  'lg' => [3, 8]
                ];
            @endphp
            <div>
                {!! BootForm::openHorizontal($columnSizes) !!}
                {!! BootForm::bind($patient) !!}
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p class="child-box-header">Thông tin bệnh nhân:</p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('Mã bệnh nhân', 'code')->addClass('input-60')->readonly() !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('* Họ Tên', 'fullname')->required()->addClass('input-60') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('* Ngày sinh', 'birthday')->required()->addClass('input-60')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '10') !!}
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-sm-offset-4 col-lg-offset-3 col-lg-3">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        {!! BootForm::radio('Nam', 'gender', 'M')->checked() !!}
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        {!! BootForm::radio('Nữ', 'gender', 'F') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('* Điện thoại', 'phone')->required()->addClass('input-60')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '11') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::email('Email', 'email')->addClass('input-60') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('Địa chỉ', 'address')->addClass('input-60') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::select('', 'province')->options($provinces)->addClass('input-60')->attribute('data-placeholder', 'Tỉnh/Thành phố') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::select('', 'district')->options($districts)->addClass('input-60')->attribute('data-placeholder', 'Quận/Huyện') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::select('', 'ward')->options($wards)->addClass('input-60')->attribute('data-placeholder', 'Phường/Xã') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::textarea('Triệu chứng (lý do khám)', 'reason')->rows(2)->addClass('input-60') !!}
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p class="child-box-header">Thông tin người thân:</p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('Bố', 'father')->addClass('input-60') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('Mẹ', 'mother')->addClass('input-60') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::text('Người giám hộ', 'other')->addClass('input-60') !!}
                            </div>
                        </div>
                        <div class="row clear-box">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p class="child-box-header">Thông tin đăng ký:</p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::select('Chuyên khoa', 'speciality')->options($specialities)->addClass('input-80') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! BootForm::select('Bác sĩ', 'doctor')->options($doctors)->addClass('input-80') !!}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-4 col-lg-3 control-label" for="date">Ngày khám</label>
                                    <div class="col-sm-7 col-lg-8">
                                        <input type="text" name="date" id="date" class="form-control input-40" onkeypress="return isNumber(event)" maxlength="10" required="required" value={!! $patient['date'] !!}>

                                        <input type="text" name="time" id="time" class="form-control input-20" maxlength="5" required="required" value={!! $patient['time'] !!}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-buttons">
                    <div class="col-xs-6 col-sm-offset-6 col-sm-6 col-md-3 col-md-offset-8 col-lg-3 col-lg-offset-9">
                        <button type="submit" class="btn">Cập nhật</button>
                        <a href="/regis">
                            <button type="button" class="btn regis-cancel">Huỷ</button>
                        </a>
                    </div>
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('js') 
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        var token = $('input[name="_token"]').attr('value');
        $('#speciality').chosen();
        $('#doctor').chosen();
        $('#province').chosen();
        // $('#district').chosen();
        // $('#ward').chosen();
        $('.regis-cancel').on('click', function() {
            $("form")[0].reset();
        });
        $('#province').on('change', function() {
            var $this = $(this);
            adaptProvinceChange($this, token);
        });

        $('#district').on('change', function() {
            var $this = $(this);
            adaptDistrictChange($this);
        });

        $('#date').datetimepicker({
            format: 'mm/dd/yyyy',
            startDate: new Date(),
            minView : 2
        });

        $('#time').datetimepicker({
            format: 'hh:ii',
            minuteStep: 15,
            // startDate: new Date(),
            formatViewType: 'time',
            fontAwesome: true,
            autoclose: true,
            startView: 1,
            maxView: 1,
            minView: 0,
        });

        $('#birthday').datetimepicker({
            format: 'mm/dd/yyyy',
            minView : 2
        });
        $('#datefrom').datetimepicker({
            format: 'mm/dd/yyyy',
            minView : 2
        });
        $('#dateto').datetimepicker({
            useCurrent: false,
            minView : 2,
            format: 'mm/dd/yyyy'
        });
        $('#birthday').bind('keyup','keydown', function(event) {
            parseDate(event);
        });
        $('#date').bind('keyup','keydown', function(event) {
            parseDate(event);
        });
    });

    function parseDate(event) {
        var inputLength = event.target.value.length;
        if(inputLength === 2 || inputLength === 5){
            var thisVal = event.target.value;
            thisVal += '/';
            $(event.target).val(thisVal);
        }
    };

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };

    function adaptProvinceChange( $this, $token ) {
        $.get('/area/district', {
            _token: $token,
            value: $this.val()
        }).done(function(data) {
            if (data) {
                $('#district').empty();
                $.each(data, function (i, item) {
                    $('#district').append($('<option>', {
                        value: item.item,
                        text : item.value
                    }));
                });

                $districtBox = $('#district');
                adaptDistrictChange($districtBox, $token);
            }
        });
    };

    function adaptDistrictChange( $this, $token ) {
        $.get('/area/ward', {
            _token: $token,
            value: $this.val(),
        }).done(function(data) {
            if (data) {
                $('#ward').empty();
                $.each(data, function (i, item) {
                    $('#ward').append($('<option>', {
                        value: item.item,
                        text : item.value
                    }));
                });
                // $('#ward').chosen();
            }
        });
    };

    function getDate( $date ) {
        var dd = $date.getDate();
        var mm = $date.getMonth()+1; //January is 0!

        var yyyy = $date.getFullYear();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        return dd + '/' + mm + '/' + yyyy;
    };
    </script>
@endsection
@extends('core.master')

@section('css')
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/basictable.css') }}" rel="stylesheet">
    <link href="https://harvesthq.github.io/chosen/chosen.css" rel="stylesheet">
	<style>	
    #table
	{
		font-family: Tahoma;
		font-size: 10px;
	}
    </style>
@endsection

@section('page-name')
Đăng ký khám chữa bệnh
@endsection

@section('main')

@php
    $globalDateFormat = 'd/m/Y';
@endphp




<div class="his-container tabs" style="min-height:1000px">
    <div class="regis-body tab-2">
	<div class="control-label">
	 <label for="tab2-1">THÔNG TIN ĐĂNG KÝ KHÁM BỆNH</label>
	</div>
       
        <input id="tab2-1" name="tabs-two" type="radio" checked="checked">
       
        <div class="row">
            {!! BootForm::open() !!}
			<div class="main-content">
			<table class="table-responsive">
                        <tbody>
                            <tr><td style="padding-left:10px"><b><u>THÔNG TIN BỆNH NHÂN</u></b></td></tr>
							
							<tr>
								<td style="padding-left:18px;padding-right:10px;padding-top:20px">
									Mã bệnh nhân :
								</td>
							</tr>
							<tr>
							    <td style="padding-left:18px;padding-top:5px;width:70%"> 
								<input type="text" name="code" id="code" class="form-control input-60 with-glyphicon" onkeypress="Javascript: if (event.keyCode==13) fnsearch(); else return isNumber(event);">
                                        <a class="search" data-url="{{ route('search') }}">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </a></td>
							</tr>
							<tr>
								<td style="padding-left:20px;padding-right:5px">* Họ Tên :</td>
							</tr>
							<tr>
								<td style="padding-right:10px;padding-left:20px">{!! BootForm::text('', 'fullname')->required()->addClass('input-100') !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:20px">* Ngày sinh :</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:20px">
									{!! BootForm::text('', 'birthday')->required()->addClass('input-100')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '10') !!}
								</td>
                            </tr>
							<tr>
							   <td style="padding-left:20px">Giới tính :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:25px">
								 {!! BootForm::radio('Nam', 'gender', 'M')->checked() !!}
								 {!! BootForm::radio('Nữ', 'gender', 'F') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px;padding-top:10px">* Điện thoại :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::text('', 'phone')->required()->addClass('input-100')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '11') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px">Email :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::email('', 'email')->addClass('input-100') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px">Địa chỉ :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::text('', 'address')->addClass('input-100') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px">Tỉnh/Thành phố :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::select('', 'province')->options($provinces)->addClass('input-100')->attribute('data-placeholder', '') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px">Quận/Huyện :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::select('', 'district')->options($districts)->addClass('input-100')->attribute('data-placeholder', '') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px">Phường/Xã :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::select('', 'ward')->options($wards)->addClass('input-100')->attribute('data-placeholder', '') !!}
							</td>
							</tr>
							
							<tr>
							   <td style="padding-left:20px">Triệu chứng (lý do khám) :</td>
							</tr>
							<tr>
							<td style="padding-right:10px;padding-left:20px">
								{!! BootForm::textarea('', 'reason')->rows(2)->addClass('input-100') !!}
							</td>
							</tr>
							
							
							
                        </tbody>
                    </table>
		</div>
		    <div class="panel">
		
		
			<table class="table-responsive">
                        <tbody>
                            <tr><td style="padding-left:10px"><b><u>THÔNG TIN NGƯỜI THÂN</u></b></td></tr>
							
							<tr>
								<td style="padding-left:20px;padding-top:10px">
									Bố :
								</td>
							</tr>
							<tr>
							    <td style="padding-left:20px;padding-top:5px;padding-right:10px"> 
								{!! BootForm::text('', 'father')->addClass('input-100') !!}
								</td>
							</tr>
							<tr>
								<td style="padding-left:20px">Mẹ :</td>
							</tr>
							<tr>
								<td style="width:800px;padding-right:10px;padding-left:20px">{!! BootForm::text('', 'mother')->addClass('input-100') !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:20px">Người giám hộ :</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:20px">
									{!! BootForm::text('', 'other')->addClass('input-100') !!}
								</td>
                            </tr>
							
							
                        </tbody>
                    </table>
		</div>
			<div class="panel">
			<table class="table-responsive">
                        <tbody>
                            <tr><td style="padding-left:10px"><b><u>THÔNG TIN ĐĂNG KÝ</u></b></td></tr>
							
							<tr>
								<td style="padding-left:20px;padding-top:10px">
									Chuyên khoa :
								</td>
							</tr>
							<tr>
							    <td style="padding-left:20px;padding-top:5px;padding-right:10px"> 
								{!! BootForm::select('', 'speciality')->options($specialities)->addClass('input-100') !!}
								</td>
							</tr>
							<tr>
								<td style="padding-left:18px">Bác sĩ :</td>
							</tr>
							<tr>
								<td style="width:800px;padding-right:10px;padding-left:20px">{!! BootForm::select('', 'doctor')->options($doctors)->addClass('input-100') !!}</td>
							</tr>
                            <tr>
                               <td style="padding-left:18px">Ngày khám :</td>
                            </tr>
							<tr>
								<td style="padding-right:10px;padding-left:20px">
									 <input type="text" name="date" id="date" class="form-control input-40" onkeypress="return isNumber(event)" maxlength="10" required="required">

                                    <input type="text" name="time" id="time" class="form-control input-20" onkeypress="return isNumber(event)" maxlength="5" readonly required="required">
								</td>
                            </tr>
							<tr>
                               <td style="padding-left:20px ;padding-top:10px"> 
							   <button type="submit" class="btn">Đăng ký</button>
							   <button  type="button" class="btn regis-cancel">Huỷ</button></td>
                            </tr>
							
                        </tbody>
                    </table>
		</div>
			
			{!! BootForm::close() !!}
        </div>
    </div>
        <div class="history tab-2" id="page">
		<div class="control-label">
			<label for="tab2-2">DANH SÁCH ĐĂNG KÝ KHÁM</label>
		</div>
            
            <input id="tab2-2" name="tabs-two" type="radio">
            <div>
			<div class="panel">
			<table class="table-responsive">
                        <tbody>
                        
							
							<tr>
							    <td style="padding-left:20px;padding-top:5px;padding-right:10px"> 
								<input type="text" name="search-name" id="search-name" placeholder="Họ tên">
								</td>
							</tr>
							
							<tr>
								<td style="width:800px;padding-right:10px;padding-left:20px;padding-top:10px"><div class='input-group date' id='datefrom'>
                                <input type='text' class="form-control" placeholder="Từ ngày" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
							</td>
							</tr>
                           
							<tr style="width:500px">
								<td style="padding-right:10px;padding-left:20px;padding-top:10px">
							<div class='input-group date' id='dateto'>
                            <input type='text' class="form-control" placeholder="đến ngày"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
								</td>
                            </tr>
							<tr>
                               <td style="padding-left:20px ;padding-top:10px;padding-bottom:10px"> 
							  <button class="btn search-list">Tìm kiếm</button>
                            </tr>
							
                        </tbody>
                    </table>
				</div>
			
			
                
                <table class="table table-bordered" id="table"">
                    <thead>
                        <th>Họ tên</th>
                        <th>Ngày sinh</th>
                        <th>Giới tính</th>
                        <th>Điện thoại</th>
                        <th>Email</th>
                        <th>Phòng khám</th>
                        <th>Chuyên khoa</th>
                        <th>Ngày khám</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @if(isset($registrations))
                            @foreach($registrations as $item)
                                <tr>
                                    <td>{{$item->patient_name }}</td>
                                    <td>{{ Carbon\Carbon::parse($item->date_birth)->format($globalDateFormat)  }}</td>
                                    <td>{{$item->gender_id == 'M' ? 'Nam' : 'Nữ' }}</td>
                                    <td>{{$item->cellphone }}</td>
                                    <td>{{$item->email }}</td>
                                    <td>{{$item->special_name }}</td>
                                    <td>{{$item->dept_id }}</td>
                                    <td>{{ Carbon\Carbon::parse($item->req_exam_date)->format($globalDateFormat) }} {{ $item->req_exam_time }}</td>
                                    <td>
                                        <a class="edit" href={{ route('get', $item->register_id) }}>
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        <a class="delete" href='#' onclick="deleteItem({{$item->register_id}}); return true;">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('js/jquery.basictable.min.js') }}"></script>
    <script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
	<script type="text/javascript">
	$('#table').basictable();
    
  </script>
    <script type="text/javascript">
    $(document).ready(function () {
        var globalFormat = 'dd/mm/yyyy';
        var token = $('input[name="_token"]').attr('value');
        $('.search').on('click', function(){
            var $this = $(this),
              url = $this.data('url'),
              code = $("#code").val();

            $.post(url, {
                _token: token,
                code: code
            }).done(function(data) {
                if (data) {
                    var obj = JSON.parse(data);
                    $("#fullname").val(obj.full_name);
                    $("#birthday").val(getDate(new Date(obj.date_of_birthday)));
                    $('input[name="gender"]').val([obj.gender]);
                    $("#address").val(obj.address);
                }
            });
        });
        $('.search-list').on('click', function(e) {
            e.preventDefault();
            var name = $('#search-name').val(),
            from = $('#datefrom > input').val(),
            to = $('#dateto > input').val();
            search(name, from, to, token);
        });
        $('#speciality').chosen();
        $('#doctor').chosen();
        $('#province').chosen();
        // $('#district').chosen();
        // $('#ward').chosen();
        $('.regis-cancel').on('click', function() {
            $("form")[0].reset();
        });
        $('#province').on('change', function() {
            var $this = $(this);
            adaptProvinceChange($this, token);
        });

        $('#district').on('change', function() {
            var $this = $(this);
            adaptDistrictChange($this);
        });

        $('#date').datetimepicker({
            format: globalFormat,
            startDate: new Date(),
            minView : 2
        });

        $('#time').datetimepicker({
            format: 'hh:ii',
            minuteStep: 15,
            // startDate: new Date(),
            formatViewType: 'time',
            fontAwesome: true,
            autoclose: true,
            startView: 1,
            maxView: 1,
            minView: 0,
        });

        $('#birthday').datetimepicker({
            format: globalFormat,
            minView : 2
        });
        $('#datefrom').datetimepicker({
            format: globalFormat,
            minView : 2
        });
        $('#dateto').datetimepicker({
            useCurrent: false,
            minView : 2,
            format: globalFormat
        });
        // $("#datefrom").on("dp.change", function (e) {
        //     $('#dateto').data("DateTimePicker").minDate(e.date);
        // });
        // $("#dateto").on("dp.change", function (e) {
        //     $('#datefrom').data("DateTimePicker").maxDate(e.date);
        // });
        $('#birthday').bind('keyup','keydown', function(event) {
            parseDate(event);
        });
        $('#date').bind('keyup','keydown', function(event) {
            parseDate(event);
        });
    });

    function adaptProvinceChange( $this, $token ) {
        $.get('/area/district', {
            _token: $token,
            value: $this.val()
        }).done(function(data) {
            if (data) {
                $('#district').empty();
                $.each(data, function (i, item) {
                    $('#district').append($('<option>', {
                        value: item.item,
                        text : item.value
                    }));
                });

                $districtBox = $('#district');
                adaptDistrictChange($districtBox, $token);
            }
        });
    };

    function adaptDistrictChange( $this, $token ) {
        $.get('/area/ward', {
            _token: $token,
            value: $this.val(),
        }).done(function(data) {
            if (data) {
                $('#ward').empty();
                $.each(data, function (i, item) {
                    $('#ward').append($('<option>', {
                        value: item.item,
                        text : item.value
                    }));
                });
                // $('#ward').chosen();
            }
        });
    };

    function search( $name, $from, $to, $token ) {
        $.post('/searchpatient', {
            _token: $token,
            name: $name,
            from: $from,
            to: $to
        }).done(function(data) {
            if (data) {
                $(".table.result > tbody > tr").remove();
                var trHTML = '';
                $.each(data, function (i, item) {
                    trHTML += '<tr><td>' + item.patient_name;
                    // trHTML += '</td><td>' + item.date_birth;
                    trHTML += '</td><td>' + getDate(new Date(item.date_birth));
                    trHTML += '</td><td>' + (item.gender_id === 'M' ? 'Nam' : 'Nữ');
                    trHTML += '</td><td>' + item.cellphone;
                    trHTML += '</td><td>' + (item.email == null ? '' : item.email);
                    trHTML += '</td><td>' + (item.special_name == null ? '' : item.special_name);
                    trHTML += '</td><td>' + (item.dept_id == null ? '' : item.dept_id);
                    trHTML += '</td><td>' + getDate(new Date(item.req_exam_date)) + ' ' + item.req_exam_time;
                    trHTML += '</td><td>' + '<a class="edit" href="/getpatient/' + item.register_id  + '">';
                    trHTML += '<span class="glyphicon glyphicon-edit"></span>';
                    trHTML += '</a>'
                    trHTML += '<a class="delete" href="#" onclick="deleteItem(' + item.register_id + '); return true;">';
                    trHTML += '<span class="glyphicon glyphicon-remove"></span>';
                    trHTML += '</a>';
                    trHTML += '</td></tr>';
                });
                $('.table.result').append(trHTML);
            }
        });
    };

    function deleteItem( $item ) {
        var token = $('input[name="_token"]').attr('value');
        if (window.confirm("Bạn có muốn xóa đăng ký này?")) {
            $.post('/delete', {
                _token: token,
                id: $item,
            }).done(function(data) {
                if (data) {
                    var name = $('#search-name').val(),
                    from = $('#datefrom > input').val(),
                    to = $('#dateto > input').val();
                    search(name, from, to, token);
                }
            });
        }
    };
    </script>
@endsection
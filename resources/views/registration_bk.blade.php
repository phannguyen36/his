<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>HIS</title>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <link href="https://harvesthq.github.io/chosen/chosen.css" rel="stylesheet">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    </head>
    <body>
        <div class="body"></div>
        <div class="his">
            <div class="his-header">
                <p class="header-name">
                    Đăng ký khám bệnh
                    <a class="logout" href="/logout">Logout</a>
                </p>
            </div>
            @php
                $globalDateFormat = 'd/m/Y';
            @endphp
            <div class="his-container tabs">
                <div class="regis-body tab-2">
                    <label for="tab2-1">Thông tin đăng ký khám bệnh</label>
                    <input id="tab2-1" name="tabs-two" type="radio" checked="checked">
                    @php
                        $columnSizes = [
                          'sm' => [4, 7],
                          'lg' => [3, 8]
                        ];
                    @endphp
                    <div>
                        {!! BootForm::openHorizontal($columnSizes) !!}
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="child-box-header">Thông tin bệnh nhân:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="col-sm-4 col-lg-3 control-label" for="code">Mã bệnh nhân</label>
                                            <div class="col-sm-6 col-lg-7">
                                                <input type="text" name="code" id="code" class="form-control input-60 with-glyphicon" onkeypress="Javascript: if (event.keyCode==13) fnsearch(); else return isNumber(event);">
                                                <div class="search-icon">
                                                    <a class="search" data-url="{{ route('search') }}">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('* Họ Tên', 'fullname')->required()->addClass('input-60') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('* Ngày sinh', 'birthday')->required()->addClass('input-60')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '10') !!}
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-sm-offset-4 col-lg-offset-3 col-lg-3">
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                {!! BootForm::radio('Nam', 'gender', 'M')->checked() !!}
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                {!! BootForm::radio('Nữ', 'gender', 'F') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('* Điện thoại', 'phone')->required()->addClass('input-60')->attribute('onkeypress', 'return isNumber(event)')->attribute('maxlength', '11') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::email('Email', 'email')->addClass('input-60') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('Địa chỉ', 'address')->addClass('input-60') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::select('', 'province')->options($provinces)->addClass('input-60')->attribute('data-placeholder', 'Tỉnh/Thành phố') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::select('', 'district')->options($districts)->addClass('input-60')->attribute('data-placeholder', 'Quận/Huyện') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::select('', 'ward')->options($wards)->addClass('input-60')->attribute('data-placeholder', 'Phường/Xã') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::textarea('Triệu chứng (lý do khám)', 'reason')->rows(2)->addClass('input-60') !!}
                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="child-box-header">Thông tin người thân:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('Bố', 'father')->addClass('input-60') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('Mẹ', 'mother')->addClass('input-60') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::text('Người giám hộ', 'other')->addClass('input-60') !!}
                                    </div>
                                </div>
                                <div class="row clear-box">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="child-box-header">Thông tin đăng ký:</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::select('Chuyên khoa', 'speciality')->options($specialities)->addClass('input-80') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {!! BootForm::select('Bác sĩ', 'doctor')->options($doctors)->addClass('input-80') !!}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="col-sm-4 col-lg-3 control-label" for="date">Ngày khám</label>
                                            <div class="col-sm-7 col-lg-8">
                                                <input type="text" name="date" id="date" class="form-control input-40" onkeypress="return isNumber(event)" maxlength="10" required="required">

                                                <input type="text" name="time" id="time" class="form-control input-20" onkeypress="return isNumber(event)" maxlength="5" readonly required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-buttons">
                            <div class="col-xs-6 col-sm-offset-6 col-sm-6 col-md-3 col-md-offset-8 col-lg-3 col-lg-offset-9">
                                <button type="submit" class="btn">Đăng ký</button>
                                <button type="button" class="btn regis-cancel">Huỷ</button>
                            </div>
                        </div>
                        {!! BootForm::close() !!}
                    </div>
                </div>
                <div class="history tab-2">
                    <label for="tab2-2">Danh sách bệnh nhân đã đăng ký</label>
                    <input id="tab2-2" name="tabs-two" type="radio">
                    <div>
                        <div class="row search-row">
                            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                <input type="text" name="search-name" id="search-name" placeholder="Họ tên">
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                <div class="form-group">
                                    <div class='input-group date' id='datefrom'>
                                        <input type='text' class="form-control" placeholder="Từ ngày" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                <div class='input-group date' id='dateto'>
                                    <input type='text' class="form-control" placeholder="đến ngày"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                <button class="btn search-list">Tìm kiếm</button>
                            </div>
                        </div>

                        <table class="table table-striped result">
                            <thead>
                                <th>Họ tên</th>
                                <th>Ngày sinh</th>
                                <th>Giới tính</th>
                                <th>Điện thoại</th>
                                <th>Email</th>
                                <th>Phòng khám</th>
                                <th>Chuyên khoa</th>
                                <th>Ngày khám</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @if(isset($registrations))
                                    @foreach($registrations as $item)
                                        <tr>
                                            <td>{{$item->patient_name }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->date_birth)->format($globalDateFormat)  }}</td>
                                            <td>{{$item->gender_id == 'M' ? 'Nam' : 'Nữ' }}</td>
                                            <td>{{$item->cellphone }}</td>
                                            <td>{{$item->email }}</td>
                                            <td>{{$item->special_name }}</td>
                                            <td>{{$item->dept_id }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->req_exam_date)->format($globalDateFormat) }} {{ $item->req_exam_time }}</td>
                                            <td>
                                                <a class="edit" href={{ route('get', $item->register_id) }}>
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </a>
                                                <a class="delete" href='#' onclick="deleteItem({{$item->register_id}}); return true;">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="his-bottom">
            <a href="http://www.freepik.com/free-photo/classmates-with-chalk-together_1249980.htm">Designed by Freepik</a>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
        <script type="text/javascript">
        $(document).ready(function () {
            var globalFormat = 'dd/mm/yyyy';
            var token = $('input[name="_token"]').attr('value');
            $('.search').on('click', function(){
                var $this = $(this),
                  url = $this.data('url'),
                  code = $("#code").val();

                $.post(url, {
                    _token: token,
                    code: code
                }).done(function(data) {
                    if (data) {
                        var obj = JSON.parse(data);
                        $("#fullname").val(obj.full_name);
                        $("#birthday").val(getDate(new Date(obj.date_of_birthday)));
                        $('input[name="gender"]').val([obj.gender]);
                        $("#address").val(obj.address);
                    }
                });
            });
            $('.search-list').on('click', function(e) {
                e.preventDefault();
                var name = $('#search-name').val(),
                from = $('#datefrom > input').val(),
                to = $('#dateto > input').val();
                search(name, from, to, token);
            });
            $('#speciality').chosen();
            $('#doctor').chosen();
            $('#province').chosen();
            // $('#district').chosen();
            // $('#ward').chosen();
            $('.regis-cancel').on('click', function() {
                $("form")[0].reset();
            });
            $('#province').on('change', function() {
                var $this = $(this);
                adaptProvinceChange($this, token);
            });

            $('#district').on('change', function() {
                var $this = $(this);
                adaptDistrictChange($this);
            });

            $('#date').datetimepicker({
                format: globalFormat,
                startDate: new Date(),
                minView : 2
            });

            $('#time').datetimepicker({
                format: 'hh:ii',
                minuteStep: 15,
                // startDate: new Date(),
                formatViewType: 'time',
                fontAwesome: true,
                autoclose: true,
                startView: 1,
                maxView: 1,
                minView: 0,
            });

            $('#birthday').datetimepicker({
                format: globalFormat,
                minView : 2
            });
            $('#datefrom').datetimepicker({
                format: globalFormat,
                minView : 2
            });
            $('#dateto').datetimepicker({
                useCurrent: false,
                minView : 2,
                format: globalFormat
            });
            // $("#datefrom").on("dp.change", function (e) {
            //     $('#dateto').data("DateTimePicker").minDate(e.date);
            // });
            // $("#dateto").on("dp.change", function (e) {
            //     $('#datefrom').data("DateTimePicker").maxDate(e.date);
            // });
            $('#birthday').bind('keyup','keydown', function(event) {
                parseDate(event);
            });
            $('#date').bind('keyup','keydown', function(event) {
                parseDate(event);
            });
        });

        function parseDate(event) {
            var inputLength = event.target.value.length;
            if(inputLength === 2 || inputLength === 5){
                var thisVal = event.target.value;
                thisVal += '/';
                $(event.target).val(thisVal);
            }
        };

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        };

        function adaptProvinceChange( $this, $token ) {
            $.get('/area/district', {
                _token: $token,
                value: $this.val()
            }).done(function(data) {
                if (data) {
                    $('#district').empty();
                    $.each(data, function (i, item) {
                        $('#district').append($('<option>', {
                            value: item.item,
                            text : item.value
                        }));
                    });

                    $districtBox = $('#district');
                    adaptDistrictChange($districtBox, $token);
                }
            });
        };

        function adaptDistrictChange( $this, $token ) {
            $.get('/area/ward', {
                _token: $token,
                value: $this.val(),
            }).done(function(data) {
                if (data) {
                    $('#ward').empty();
                    $.each(data, function (i, item) {
                        $('#ward').append($('<option>', {
                            value: item.item,
                            text : item.value
                        }));
                    });
                    // $('#ward').chosen();
                }
            });
        };

        function search( $name, $from, $to, $token ) {
            $.post('/searchpatient', {
                _token: $token,
                name: $name,
                from: $from,
                to: $to
            }).done(function(data) {
                if (data) {
                    $(".table.result > tbody > tr").remove();
                    var trHTML = '';
                    $.each(data, function (i, item) {
                        trHTML += '<tr><td>' + item.patient_name;
                        // trHTML += '</td><td>' + item.date_birth;
                        trHTML += '</td><td>' + getDate(new Date(item.date_birth));
                        trHTML += '</td><td>' + (item.gender_id === 'M' ? 'Nam' : 'Nữ');
                        trHTML += '</td><td>' + item.cellphone;
                        trHTML += '</td><td>' + (item.email == null ? '' : item.email);
                        trHTML += '</td><td>' + (item.special_name == null ? '' : item.special_name);
                        trHTML += '</td><td>' + (item.dept_id == null ? '' : item.dept_id);
                        trHTML += '</td><td>' + getDate(new Date(item.req_exam_date));
                        trHTML += '</td><td>' + '<a class="edit" href="/getpatient/' + item.register_id  + '">';
                        trHTML += '<span class="glyphicon glyphicon-edit"></span>';
                        trHTML += '</a>'
                        trHTML += '<a class="delete" href="#" onclick="deleteItem(' + item.register_id + '); return true;">';
                        trHTML += '<span class="glyphicon glyphicon-remove"></span>';
                        trHTML += '</a>';
                        trHTML += '</td></tr>';
                    });
                    $('.table.result').append(trHTML);
                }
            });
        };

        function getDate( $date ) {
            var dd = $date.getDate();
            var mm = $date.getMonth()+1; //January is 0!

            var yyyy = $date.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            return dd + '/' + mm + '/' + yyyy;
        };

        function deleteItem( $item ) {
            var token = $('input[name="_token"]').attr('value');
            if (window.confirm("Bạn có muốn xóa đăng ký này?")) {
                $.post('/delete', {
                    _token: token,
                    id: $item,
                }).done(function(data) {
                    if (data) {
                        var name = $('#search-name').val(),
                        from = $('#datefrom > input').val(),
                        to = $('#dateto > input').val();
                        search(name, from, to, token);
                    }
                });
            }
        };

        function fnsearch() {
            $('.search').click();
        };
        </script>
    </body>
</html>
